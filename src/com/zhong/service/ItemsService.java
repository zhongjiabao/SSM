package com.zhong.service;


import com.zhong.pojo.Items;

import java.util.List;

public interface ItemsService {


    public List<Items> list() throws Exception;

    public Items findItemsById(Integer id) throws Exception;

    public void updateItems(Items items) throws Exception;
}
